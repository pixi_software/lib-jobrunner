# Lib: JobRunner

-----
The JobRunner library will assist you when creating the jobs that will be executed by an cronjob. By using Symfonys expression language it's very simple to have a quick overview when a specific function will be called or not.

Besides setting up single jobs will give you an better overview inside the code, you will also see detailed information about the consumed memory and time spent for each function that will be exectuted by the JobRunner. This way you can identify which functions might need an update.

## Quick Start

-----

To run the example application, you must first install the development dependencies via composer. From the root `Lib - JobRunner`, run:

	$ php composer install

After including the `autoload.php` into your project you can start using the JobRunner.

To setup your first job you can use following code snippet:
```
:::PHP
<?php

/**
 * @ttl int Maximum execution time for this job
 * @function string Function that is beeing called (note you can use native php functions like `microtime`
 * @name string Name of the job so you can identify it later
$job = new Job(['ttl' => 120, 'function' => 'microtime', 'name' => 'test-job2']);

?>
```

A single job won't work for you, because you need to add it to the JobRunner itself.
```
:::PHP
<?php

$jobRunner = new \Pixi\Cronjob\JobRunner();
$jobRunner->setPersistance(new \Pixi\Cronjob\Persistance\ActiveRecordAdapter($this->customerdb)); // Asigne adapter to write the logs into the database

$job = new Job(['ttl' => 120, 'function' => 'microtime', 'name' => 'test-job2']);

$jobRunner->addJob($job);

?>
```

Runnin the job in the end is done be using `execute`
```
:::PHP
<?php

...

$jobRunner->addJob($job);
$jobRunner->execute();

?>
```

> ***NOTE:*** For more details visit the library [wiki](https://bitbucket.org/pixi_software/lib-jobrunner/wiki/Home)