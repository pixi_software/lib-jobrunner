<?php

use Symfony\Component\ExpressionLanguage\Expression;
use Pixi\Cronjob\Cron;
use Pixi\Cronjob\Job;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Pixi\Cronjob\JobResponse;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Pixi\Cronjob\Persistance\DoctrineDbalAdapter;
use Pixi\Cronjob\JobRunner;

require_once 'vendor/autoload.php';

class Apple implements LoggerAwareInterface
{
    public $variety;
    public $testArray = array('some', 'apple');
    public $logger;

    public function __construct()
    {

        //$this->testArray['some'] = new stdClass();
        //$this->testArray['some']->name = 'Granny';
        //$this->testArray['some']->surname = function() { return 'Shmitt'; };

    }
    
    public function setLogger(LoggerInterface $logger) 
    {
        $this->logger = $logger;
    }
    
    public function string()
    {
        
        sleep(1);
        
        $this->logger->info('all kull and executed', ['some text']);
        
        //throw new \Exception('not kull');
        
        return new JobResponse(JobResponse::SUCCESS, 'Yeah baby', implode(', ', $this->testArray));
        
    }

}

$job1 = new Job(array('name' => 'date-job-1', 'context' => '\DateTime', 'function' => 'format', 'args' => array('d.m.Y H:i:s'), 'concurrency' => 1));

$job2 = new Job(array('name' => 'date-job-2', 'context' => null, 'function' => function($some) { return number_format($some); }, 'args' => 500));

$job3 = new Job(array('name' => 'date-job-3', 'context' => false, 'function' => 'number_format', 'args' => 500));

$job4 = new Job(array('name' => 'date-job-4', 'context' => 'Apple', 'function' => 'string', 'breakOnError' => false));
$job4->info['customer'] = 'APP';
$job4->addExpression('customer-name',  'context.info["customer"]', 'APP');
$job4->addExpression('last-execution', 'stats.date("U") - stats.lastSuccessfulJobDate() > 6', true);

$cron = new JobRunner();

$cron->setPersistance(new DoctrineDbalAdapter('cron_test', 'root'));

$cron->addJob($job1);
$cron->addJob($job2);
$cron->addJob($job3);
$cron->addJob($job4);

$cron->execute();

print_r($cron->jobExpressionFailures);

/*
print_r($cron->jobReturnValues);
print_r($cron->jobLogs);
print_r($cron->jobStatistics);

print_r($cron->getOptions());

print_r($cron->extract());
*/

/*


$cron->addExpression('me is kul');

$job1 = new Job(array('concurrency' => 2));

$cron->addJob($job1);

print_r($cron);
*/

/*
$language = new ExpressionLanguage();



$apple = new Apple();

$apple->variety = 'Honeycrisp';

echo $language->evaluate(
    'fruit.testArray["some"].surname()',
    array(
        'fruit' => $apple,
    )
);
*/