<?php

namespace Pixi\Cronjob;

trait ExpressionTrait
{
    
    protected $expressions = array(
        'concurrency' => ['expression' => 'stats.getActiveCount() < context.getConcurrency()', 'value' => true],
        //'fake' => ['expression' => 'context.getName() != "date-job-1"', 'value' => true]
        //'time' => ['expression' => 'stats.date("H")', 'value' => 18]
    );

    public function getExpressions()
    {
        return $this->expressions;
    }

    public function setExpressions(array $expressions)
    {
        $this->expressions = $expressions;
        return $this;
    }
 
    public function addExpression($name, $expression, $value = true)
    {
        $this->expressions[$name] = ['expression' => $expression, 'value' => $value];
    }
    
}
