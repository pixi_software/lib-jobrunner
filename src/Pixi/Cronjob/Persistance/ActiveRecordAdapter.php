<?php
namespace Pixi\Cronjob\Persistance;

class ActiveRecordAdapter implements PersistanceInterface
{

    public $conn;

    public $cronTable = 'jobrunner_cron';

    public $jobTable = 'jobrunner_job';
    
    public $tables = array(
        
        'jobrunner_job' => "CREATE TABLE IF NOT EXISTS `jobrunner_job` (
          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
          `id_cron` bigint(20) unsigned NOT NULL,
          `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
          `data` mediumtext COLLATE utf8_unicode_ci,
          `error` tinyint(3) unsigned NOT NULL,
          `killed` tinyint(3) unsigned DEFAULT '0',
          `ttl` int(10) unsigned NOT NULL,
          `date_start` datetime NOT NULL,
          `date_end` datetime DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `id_cron` (`id_cron`),
          KEY `name` (`name`,`date_start`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;",
        
        'jobrunner_cron' => "CREATE TABLE IF NOT EXISTS `jobrunner_cron` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
          `data` mediumtext COLLATE utf8_unicode_ci,
          `report` mediumtext COLLATE utf8_unicode_ci,
          `pid` int(10) unsigned DEFAULT NULL,
          `ttl` mediumint(8) unsigned DEFAULT NULL,
          `killed` tinyint(3) unsigned DEFAULT '0',
          `date_start` datetime DEFAULT NULL,
          `date_end` datetime DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `name` (`name`,`date_start`),
          KEY `clearkey` (`killed`,`date_end`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"
        
    );

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    public function setupTables()
    {
        
        foreach($this->tables as $table) {
            $this->conn->query($table);
        }
        
    }
    
    public function query($query, $values = false)
    {
    
        if($values AND is_array($values)) {
            $this->conn->query($query, $values);
        } else {
            $this->conn->query($query);
        }
    
        return $this->conn->affected_rows();
        
    }
    
    public function getCronsByName($name)
    {
        return $this->conn->query("SELECT id, name, ttl, pid, killed, date_start, date_end FROM $this->cronTable
            WHERE name = ?
            ORDER BY date_start DESC LIMIT 0, 10", array($name))->result_array();
    
    }
    
    public function createCron($data)
    {
        $this->conn->insert($this->cronTable, $data);
        return $this->conn->insert_id();
    }

    public function updateCron($data)
    {
        if (isset($data['id']) and $data['id'] > 0) {
            return $this->conn->update($this->cronTable, $data, array(
                'id' => $data['id']
            ));
        }
        return false;
    }
    
    public function killCrons($ids)
    {
        
        if(is_array($ids)) {
        
            $date_end = date('Y-m-d H:i:s');
        
            foreach($ids as $id) {
        
                $this->conn->update($this->cronTable, array('killed' => 1, 'date_end' => $date_end), array('id' => $id));
        
            }
        
            return true;
        
        }
        
        return false;
        
    }

    public function createJob($data)
    {
        $this->conn->insert($this->jobTable, $data);
        return $this->conn->insert_id();
    }

    public function updateJob($data)
    {
                
        if (isset($data['id']) and $data['id'] > 0) {
            return $this->conn->update($this->jobTable, $data, array(
                'id' => $data['id']
            ));
        }
        
        return false;
    }
    
    public function getJobsByName($name)
    {
        
        return $this->conn->query("SELECT id, id_cron, name, killed, error, ttl, date_start, date_end FROM $this->jobTable 
            WHERE name = ?
            ORDER BY date_start DESC LIMIT 0, 10", array($name))->result_array();
        
    }
    
    public function killJobs($ids)
    {

        if(is_array($ids)) {
            
            $date_end = date('Y-m-d H:i:s');
            
            foreach($ids as $id) {
                
                $this->conn->update($this->jobTable, array('killed' => 1, 'date_end' => $date_end), array('id' => $id));
            
            }

            return true;
            
        }
        
        return false;
                
    }
    
}
