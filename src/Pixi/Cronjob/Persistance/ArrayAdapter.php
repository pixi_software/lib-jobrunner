<?php
namespace Pixi\Cronjob\Persistance;

class ArrayAdapter
{

    public $cronTable;

    public $jobTable;

    public function __construct($crons = array(), $jobs = array())
    {
        
        $this->cronTable = $crons;
        
        $this->jobTable = $jobs;
        
    }

    public function getCronsByName($name)
    {
    
        return $this->cronTable;
    
    }
    
    public function createCron($data)
    {
        $id = count($this->cronTable) + 1;
        $this->cronTable[$id] = $data;
        return $id;
    }

    public function updateCron($data)
    {
        if (isset($data['id']) and $data['id'] > 0) {
            $this->cronTable[$data['id']] = $data;
            return true;
        }
        return false;
    }
    
    public function killCrons($ids)
    {
        
        if(is_array($ids)) {
        
            $date_end = date('Y-m-d H:i:s');
        
            foreach($ids as $id) {
                
                $this->cronTable[$id]['killed'] = 1;
                $this->cronTable[$id]['date_end'] = $date_end;
        
            }
        
            return true;
        
        }
        
        return false;
        
    }

    public function createJob($data)
    {
        $id = count($this->jobTable) + 1;
        $this->jobTable[$id] = $data;
        return $id;
    }

    public function updateJob($data)
    {
        if (isset($data['id']) and $data['id'] > 0) {
            $this->jobTable[$data['id']] = $data;
            return true;
        }
        
        return false;
    }
    
    public function getJobsByName($name)
    {
        
        $temp = array();
        
        foreach($this->jobTable as $job) {
            
            if($job['name'] == $name) {
                $temp[] = $job;
            }
            
        }
        
        return $temp;
        
    }
    
    public function killJobs($ids)
    {

        if(is_array($ids)) {
            
            $date_end = date('Y-m-d H:i:s');
            
            foreach($ids as $id) {
                $this->jobTable[$id]['killed'] = 1;
                $this->jobTable[$id]['date_end'] = $date_end;
            }

            return true;
            
        }
        
        return false;
                
    }
    
}
