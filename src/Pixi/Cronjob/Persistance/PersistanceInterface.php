<?php
namespace Pixi\Cronjob\Persistance;

interface PersistanceInterface
{

    public function setupTables();

    public function createJob($job);

    public function updateJob($job);

    public function getJobsByName($name);

    public function killJobs($ids);

    public function createCron($cron);

    public function updateCron($cron);

    public function getCronsByName($name);

    public function killCrons($ids);
    
    public function query($query, $values = false);
    
}
