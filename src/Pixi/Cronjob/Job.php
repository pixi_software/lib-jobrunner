<?php

namespace Pixi\Cronjob;

use Pixi\Cronjob\Exception\JobException;
use Psr\Log\LoggerAwareInterface;
use Evenement\EventEmitterTrait;

class Job
{
    
    use ExpressionTrait, EventEmitterTrait;
    
    public $returnValue;
    
    public $logger = false;
    
    public $exception = false;
        
    public $valid = false;
    
    public $dateStart = false;
    
    public $dateEnd = false;
    
    private $id;
    
    private $id_cron;
    
    public $info = array();
    
    public $statistics = array(
        'execution.name'    => '',
        'date.start'        => 0,
        'date.end'          => 0,
        'time.total'        => '0.00 sec',
        'memory.start'      => 0,
        'memory.end'        => 0,
        'memory.usage'      => '0 kb',
        'executed'          => 0,
        'has.error'         => 0,
        'error.message'     => ''
    );
    
    public $options = array(
        'name'          => false,
        'context'       => false,
        'contextType'   => false,
        'function'      => false,
        'args'          => false,
        'breakOnError'  => false,
        'concurrency'   => 1,
        'ttl'           => 120,
        'id_cron'       => false,
        'add_context'   => false
    );
    
    public function __construct(array $options)
    {
    
        $this->setOptions($options);
        
        $this->init();
    
        return $this;
    
    }
    
    public function init()
    {
    
        if(is_object($this->options['context'])) {
            
            $this->options['contextType'] = 'Object';
            $this->statistics['execution.name'] = get_class($this->options['context']) . '::' . $this->options['function'];
            $this->valid = true;
            
        } elseif(is_string($this->options['context']) AND class_exists($this->options['context'], true)) {
               
            $this->options['context'] = new $this->options['context'];
            $this->options['contextType'] = 'Object';
            $this->statistics['execution.name'] = get_class($this->options['context']) . '::' . $this->options['function'];
            $this->valid = true;
             
        } elseif(!$this->options['context'] AND is_string($this->options['function']) AND function_exists($this->options['function'])) {
            
            $this->options['contextType'] = 'Function';
            $this->statistics['execution.name'] = $this->options['contextType'] . '::' . $this->options['function'];
            $this->valid = true;
            
        } elseif(!$this->options['context'] AND is_callable($this->options['function'])) {
            
            $this->options['contextType'] = 'Closure';
            $this->statistics['execution.name'] = $this->options['contextType'] . '::' . 'anonymous';
            $this->valid = true;
            
        } else {
            
            $this->options['contextType'] = false;
            $this->valid = false;
            
        }
        
        if($this->options['contextType'] == 'Object' AND $this->options['context'] instanceof LoggerAwareInterface) {
            $this->options['context']->setLogger($this->getLogger());
        }
    
    }
    
    public function extract($omit = array('data'))
    {
        
        $data = array(
            'name'          => $this->getName(),
            'id_cron'       => $this->getIdCron(),
            'date_start'    => $this->dateStart,
            'ttl'           => $this->getTTL(),
            'error'         => $this->hasError(),
            'data'          => json_encode(array(
                'statistic'     => $this->getStatistics(),
                'log'           => $this->getLogEntries(),
                'returnValue'   => $this->getReturnValue(),
                'options'       => $this->getOptions()
            ))
        );
                
        if($this->dateEnd) {
            $data['date_end'] = $this->dateEnd;
        }
        
        if($this->getId()) {
            $data['id'] = $this->getId();
        }
                
        if($omit AND is_array($omit)) {
            foreach($omit as $key) {
                if(isset($data[$key])) {
                    unset($data[$key]);
                }
            }
        }
        
        return $data;
        
    }
    
    public function preRun()
    {
        $this->dateStart = date('Y-m-d H:i:s');
        $this->emit('run.pre', [$this]);
    }
    
    public function run()
    {
    
        $this->preRun();
        
        $this->statistics['date.start'] = microtime(true);
        $this->statistics['memory.start'] = memory_get_usage();
        
        set_time_limit(600);

        // log start
        try {
    
            if(isset($this->options['args']['throwException']) AND $this->options['args']['throwException']) {
                throw new JobException($this->options['args']['throwException']);
            }
    
            switch($this->options['contextType']) {
                
                case 'Object':
                    
                    if($this->options['args']) {
                        $this->returnValue = call_user_func_array(array($this->options['context'], $this->options['function']), $this->options['args']);
                    } else {
                        $this->returnValue = call_user_func(array($this->options['context'], $this->options['function']));
                    }
                    
                    break;
                    
                case 'Closure':
                case 'Function':

                    if(is_array($this->options['args'])) {
                        $this->returnValue = call_user_func_array($this->options['function'], $this->options['args']);
                    } else {
                        $this->returnValue = call_user_func($this->options['function']);
                    }
                    
                    break;
                
                default:
                    $this->returnValue = new JobResponse(JobResponse::NOT_EXECUTED, 'Job not executed');
                    break;
                    
            }
            
        } catch (\Exception $e) {
             
            $this->returnValue = new JobResponse(JobResponse::EXCEPTION, $e->getMessage(), ['exception' => $e]);
            
            $this->statistics['has.error'] = 1;
            $this->statistics['error.message'] = $e->getMessage();
            
            $this->getLogger()->alert($e->getMessage());
    
        }
        
        // log finish
        $this->statistics['date.end'] = microtime(true);
        $this->statistics['memory.end'] = memory_get_usage();
        
        // some cleanup
        if(is_object($this->returnValue) AND $this->returnValue instanceof JobResponse) {
            
            if($this->returnValue->getStatus() != JobResponse::SUCCESS AND $this->returnValue->getStatus() != JobResponse::EXCEPTION) {
                $this->statistics['has.error'] = 1;
                $this->statistics['error.message'] = $this->returnValue->getMessage();
                $this->getLogger()->alert($this->returnValue->getMessage());
            }
            
        } else {
            
            $this->returnValue = new JobResponse(JobResponse::SUCCESS, 'Executed', $this->returnValue);
        
        }
        
        $this->postRun();
    
    }
    
    public function postRun()
    {
        
        $this->dateEnd = date('Y-m-d H:i:s');
        
        $this->statistics['executed'] = true;
    
        if($this->statistics['memory.end'] - $this->statistics['memory.start'] < 500) {
            $this->statistics['memory.usage'] = '0 kb';
        } else {
            $this->statistics['memory.usage'] = round(($this->statistics['memory.end'] - $this->statistics['memory.start']) / 1024) . ' kb';
        }

        $this->statistics['time.total'] = round($this->statistics['date.end'] - $this->statistics['date.start'], 2) . ' sec';
        
        $this->emit('run.post', [$this]);
        
    }
    
    public function getOptions()
    {
        
        if($this->options['add_context']) {
            return $this->options;
        }
        
        $opt = $this->options;
        unset($opt['context']);
        return $opt;
        
    }
    
    public function setOptions($options)
    {
        $this->options = array_merge($this->options, $options);
        return $this;
    }
    
    public function getName()
    {
        return $this->options['name'];
    }
    
    public function getReturnValue()
    {
        return $this->returnValue;
    }
    
    public function getStatistics()
    {
        return $this->statistics;
    }
    
    public function getConcurrency()
    {
        return $this->options['concurrency'];
    }
    
    public function setConcurrency($concurrency)
    {
        $this->options['concurrency'] = $concurrency;
        return $this;
    }
    
    public function getLogger()
    {
         
        if(!$this->logger) {
            $this->logger = new ArrayLogger();
        }
    
        return $this->logger;
    
    }
    
    public function getLogEntries()
    {
        return $this->getLogger()->getFormattedLogs();
    }
    
    public function isValid()
    {
        if($this->valid AND $this->options['name']) {
            return true;
        } else {
            return false;
        }
    }
    
    public function breakOnError()
    {
        return $this->options['breakOnError'];
    }
    
    public function getTTL()
    {
        return $this->options['ttl'];
    }
    
    public function setTTL($ttl)
    {
        
        if(is_numeric($ttl) AND $ttl > 0) {
            $this->options['ttl'] = $ttl;
        }
        
        return $this;
        
    }
    
    public function setIdCron($id)
    {
        $this->options['id_cron'] = $id;
        return $this;
    }
    
    public function getIdCron()
    {
        return $this->options['id_cron'];
    }
    
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function hasError()
    {
        
        if($this->statistics['has.error'] == 1) {
            return true;
        } else {
            return false;
        }
        
    }
    
}
