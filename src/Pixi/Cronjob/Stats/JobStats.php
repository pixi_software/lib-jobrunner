<?php
namespace Pixi\Cronjob\Stats;

class JobStats
{

    public $jobs = array();

    public function getActiveCount()
    {
        $count = 0;
        foreach ($this->jobs as $job) {
            if ($job['date_end'] == '') {
                $count ++;
            }
        }
        return $count;
    }

    public function setJobs(array $jobs)
    {
        $this->jobs = $jobs;
    }

    public function date($format = 'U')
    {
        return date($format);
    }

    public function lastSuccessfulJobDate($format = 'U')
    {
        
        $refDate = new \DateTime('1970-01-01');

        foreach ($this->jobs as $job) {
            if ($job['killed'] == 0 AND $job['error'] == 0 AND $job['date_start'] != '' AND $job['date_end'] != '') {
                $date = \DateTime::createFromFormat('Y-m-d H:i:s', $job['date_start']);
                if ($date->getTimestamp() > $refDate->getTimestamp()) {
                    $refDate = $date;
                }
            }
        }
        
        return $refDate->format($format);
        
    }
    
    public function killableJobs()
    {
        
        $ids = false;
        
        foreach($this->jobs as $job) {
            
            if(empty($job['date_end'])) {
                
                $date_start = \DateTime::createFromFormat('Y-m-d H:i:s', $job['date_start']);
                
                if(date('U') - $date_start->getTimestamp() > $job['ttl']) {
                    $ids[] = $job['id'];
                }
                
            }
            
        }
        
        return $ids;
        
    }
    
}
