<?php
namespace Pixi\Cronjob\Stats;

class JobRunnerStats
{

    public $crons = array();

    public function getActiveCount()
    {
        $count = 0;
        foreach ($this->crons as $cron) {
            if ($cron['date_end'] == '') {
                $count ++;
            }
        }
        return $count;
    }

    public function setCrons(array $crons)
    {
        $this->crons = $crons;
    }

    public function date($date = null)
    {
        return date($date);
    }
    
    public function killableCrons()
    {
        
        $ids = false;
        
        foreach($this->crons as $cron) {
            
            if(empty($cron['date_end'])) {
                
                $date_start = \DateTime::createFromFormat('Y-m-d H:i:s', $cron['date_start']);
                
                if(date('U') - $date_start->getTimestamp() > $cron['ttl']) {
                    $ids[] = $cron['id'];
                }
                
            }
            
        }
        
        return $ids;
        
    }
    
}
