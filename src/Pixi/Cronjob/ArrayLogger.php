<?php

namespace Pixi\Cronjob;

class ArrayLogger extends \Psr\Log\AbstractLogger
{
	
    public $logs = array();
    
    public function log($level, string|\Stringable $message, array $context = []): void
    {
    	$this->logs[] = array(microtime(true), $level, $message, $context);
    }
    
    public function getLogs()
    {
    	return $this->logs;
    }
    
    public function getFormattedLogs()
    {
    	
        $tmp = array();
        
        foreach($this->logs as $log) {
        	$tmp[] = array(
        		'date' => date('d.m.Y H:i:s', $log[0]),
        	    'level' => $log[1],
        	    'message' => $log[2],
        	    'context' => count($log[3]) > 0 ? json_encode($log[3]) : ''
        	);
        }

        return $tmp;
        
    }
    
}
