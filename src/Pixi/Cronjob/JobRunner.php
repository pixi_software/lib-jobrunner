<?php

namespace Pixi\Cronjob;

use Pixi\Cronjob\Exception\JobRunnerException;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Pixi\Cronjob\Stats\JobStats;
use Pixi\Cronjob\Stats\JobRunnerStats;
use Pixi\Cronjob\Persistance\PersistanceInterface;

class JobRunner
{
    
    use ExpressionTrait;
    
    public $jobQueue;
    
    public $jobStatistics = array();
    
    public $jobReturnValues = array();
    
    public $jobLogs = array();
    
    public $jobExpressionFailures = array();
    
    /**
     * @var PersistanceInterface
     */
    public $persistance;
    
    public $dateStart = false;
    
    public $dateEnd = false;
    
    private $id;
    
    private $pid;
    
    private $language;
    
    private $options = array(
        'ttl'           => 120,
        'name'          => 'standart-runner',
        'log-data'      => false,
        'concurrency'   => 1
    );
        
    public function __construct(array $options = array())
    {
        
        $this->jobQueue = new JobQueue;
        
        $this->language = new ExpressionLanguage();
        
        $this->pid = getmypid();
        
        $this->setOptions($options);
        
    }
    
    public function addJob(Job $job)
    {
        
        if($job->isValid()) {
            
            $job->on('run.pre', [$this, 'jobPreRun']);
            $job->on('run.post', [$this, 'jobPostRun']);
            
            $this->setTTL($this->getTTL() + $job->getTTL());
            
            $this->jobQueue->enqueue($job);
            
        } else {
            
            throw new JobRunnerException('Job not valid');
            
        }
                
    }
    
    public function jobPreRun(Job $job)
    {
        
        $job->setIdCron($this->getId());
        $job->setId($this->persistance->createJob($job->extract()));
        
    }
    
    public function jobPostRun(Job $job)
    {
        if($job->hasError()) {
            $this->persistance->updateJob($job->extract(false));
        } else {
            $this->persistance->updateJob($job->extract());
        }
    }
    
    public function extract()
    {
        
        $data = array(
            'name'          => $this->getName(),
            'date_start'    => $this->dateStart,
            'pid'           => $this->pid,
            'ttl'           => $this->getTTL()
        );
        
        if($this->getId()) {
            $data['id'] = $this->getId();
        }
        
        if($this->dateEnd) {
            $data['date_end'] = $this->dateEnd;
        }
        
        if(count($this->jobStatistics) > 0) {
            
            $data['report'] = $this->getReport();
            
            if($this->options['log-data']) {
                $data['data'] = json_encode(array(
                    'jobStatistics'         => $this->jobStatistics,
                    'jobLogs'               => $this->jobLogs,
                    'jobReturnValues'       => $this->jobReturnValues,
                    'jobExpressionFailures' => $this->jobExpressionFailures
                ));
            }
            
        }
        
        return $data;
        
    }
    
    public function canExecuteRunner()
    {

        $cronStats = new JobRunnerStats();
        
        $cronStats->setCrons($this->persistance->getCronsByName($this->getName()));
        
        if($this->persistance->killCrons($cronStats->killableCrons())) {
            $cronStats->setCrons($this->persistance->getCronsByName($this->getName()));
        }
        
        $failures = array();
        
        foreach($this->getExpressions() as $name => $expression) {
            $val = $this->language->evaluate($expression['expression'], ['context' => $this, 'stats' => $cronStats]);
            if($val != $expression['value']) {
                $failures[$name] = "$val != " . $expression['value'];
            }
        }
        
        if(count($failures) > 0) {
            return false;
        } else {
            return true;
        }
        
    }
    
    public function preExecute()
    {
        
        $this->dateStart = date('Y-m-d H:i:s');
        $this->setId($this->persistance->createCron($this->extract()));
        
    }
    
    public function execute()
    {
        
        if(!$this->canExecuteRunner()) {
            return;
        }
        
        $this->preExecute();
        
        foreach($this->jobQueue as $job) {
            
            if(!$this->canExecuteJob($job)) {
                
                $this->jobStatistics[$job->getName()] = $job->getStatistics();
                $this->setTTL($this->getTTL() - $job->getTTl());
                $this->persistance->updateCron($this->extract());
                
            } else {

                $job->run();
                
                $this->jobStatistics[$job->getName()] = $job->getStatistics();
                $this->jobLogs[$job->getName()] = $job->getLogEntries();
                $this->jobReturnValues[$job->getName()] = $job->getReturnValue();
                $this->persistance->updateCron($this->extract());
                
                if($job->getReturnValue()->getStatus() != JobResponse::SUCCESS AND $job->breakOnError()) {
                    break;
                }
                
            }
            
        }
        
        $this->postExecute();
        
    }
    
    public function canExecuteJob(Job $job)
    {
        
        $jobStats = new JobStats();
        
        $jobStats->setJobs($this->persistance->getJobsByName($job->getName()));
        
        if($this->persistance->killJobs($jobStats->killableJobs())) {
            $jobStats->setJobs($this->persistance->getJobsByName($job->getName()));
        }
        
        $failures = array();
        
        foreach($job->getExpressions() as $name => $expression) {
            $val = $this->language->evaluate($expression['expression'], ['context' => $job, 'stats' => $jobStats]);
            if($val != $expression['value']) {
                $failures[$name] = "$val != " . $expression['value'];
            }
        }

        if(count($failures) > 0) {
            $this->jobExpressionFailures[$job->getName()] = $failures;
            return false;
        } else {
            return true;
        }
        
    }
    
    public function postExecute()
    {
        $this->dateEnd = date('Y-m-d H:i:s');
        $this->persistance->updateCron($this->extract());
    }
    
    public function getReport()
    {
    
        $table = new \Zend\Text\Table\Table(array('columnWidths' => array(30, 14, 12, 7, 20, 10), 'decorator' => 'ascii', 'padding' => 1));
        
        $table->appendRow(array('Job Name', 'Memory Usage', 'Sec. Spent', 'Error', 'Error Message', 'Executed'));
         
        foreach($this->jobStatistics as $stat) {
            $table->appendRow([$stat['execution.name'], $stat['memory.usage'], $stat['time.total'], $stat['has.error'] . '', $stat['error.message'], $stat['executed'] . '']);
        }

        return $table->__toString();

    }

    public function getName()
    {
        return $this->options['name'];
    }

    public function setName($name)
    {
        $this->options['name'] = $name;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    
    public function getTTL()
    {
        return $this->options['ttl'];
    }
    
    public function setTTL($ttl)
    {
        if(is_numeric($ttl) AND $ttl > 0) {
            $this->options['ttl'] = $ttl;
        }
        
        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        
        foreach($options as $k => $v) {
            
            switch($k) {
                
                case 'name':
                    $this->setName($v);
                    unset($options[$k]);
                    break;
                    
                case 'persistance': 
                    $this->setPersistance($v);
                    unset($options[$k]);
                    break;
                    
                case 'ttl':
                    $this->setTTL($v);
                    unset($options[$k]);
                    break;
                    
            }
            
        }
        
        $this->options = array_merge($this->options, $options);
        return $this;
    }

    public function getPersistance()
    {
        return $this->persistance;
    }

    public function setPersistance($persistance)
    {
        $this->persistance = $persistance;
        return $this;
    }
    
    public function getConcurrency()
    {
        return $this->options['concurrency'];
    }
    
    public function isExecuted()
    {
        if(!$this->dateEnd AND !$this->dateStart) {
            return false;
        } else {
            return true;
        }
    }
 
}
