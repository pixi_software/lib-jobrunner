<?php

namespace Pixi\Cronjob;

class JobResponse implements \JsonSerializable 
{

    /**
     * @var Successfully executed
     */
    const SUCCESS = 1;
    
    /**
     * @var Error returned
     */
    const ERROR = 2;
    
    /**
     * @var Error returned, retry
     */
    const RETRY = 3;
    
    /**
     * @var Exception thrown
     */
    const EXCEPTION = 4;
    
    /**
     * @var Not executed
     */
    const NOT_EXECUTED = 5;
    
    protected $status;
    protected $message;
    protected $returnValue;
    
    public function __construct($status = 1, $message = false, $returnValue = false)
    {
        
        $this->setStatus($status);
        
        $this->setMessage($message);
        
        $this->setReturnValue($returnValue);        
        
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getReturnValue()
    {
        return $this->returnValue;
    }

    public function setReturnValue($returnValue)
    {
        $this->returnValue = $returnValue;
        return $this;
    }
    
    public function jsonSerialize()
    {
        
        return array(
            'status'        => $this->getStatus(),
            'message'       => $this->getMessage(),
            'returnValue'   => $this->getReturnValue()
        );
        
    }
     
}
