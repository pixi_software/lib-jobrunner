<?php
namespace Pixi\Cronjob\Job;

use Pixi\Cronjob\Job;
use Pixi\Cronjob\JobResponse;
use Pixi\Cronjob\Persistance\PersistanceInterface;

class ClearCronLogs extends Job
{

    public $okDays = 10;

    public $killedDays = 30;

    public $timeExpression = [
        'name'          => 'once-daily',
        'expression'    => 'stats.date() - stats.lastSuccessfulJobDate() > 86400',
        'value'         => true
    ];

    public function __construct($conn)
    {
        
        $this->addExpression(
            $this->timeExpression['name'], 
            $this->timeExpression['expression'], 
            $this->timeExpression['value']
        );
        
        $options = [
            'name'      => 'clear-cron-logs', 
            'function'  => 'clearLogs',
            'context'   => $this,
            'args'      => [$conn]
        ];
        
        return parent::__construct($options);
        
    }

    public function clearLogs($conn)
    {
        
        if($conn instanceof PersistanceInterface) {
            
            $msg = ['okDays' => $this->okDays, 'killedDays' => $this->killedDays];
            
            $dateLog = date('Y-m-d', strtotime('-' . $this->okDays . ' days'));
            
            $msg['goodCrons'] = $conn->query('DELETE FROM jobrunner_cron WHERE killed = 0 AND date_end < ?', array($dateLog));
            
            $dateKilled = date('Y-m-d', strtotime('-' . $this->killedDays . ' days'));
            
            $msg['killedCrons'] = $conn->query('DELETE FROM jobrunner_cron WHERE killed = 1 AND date_end < ?', array($dateKilled));
            
            $msg['jobs'] = $conn->query('DELETE FROM jobrunner_job WHERE id_cron NOT IN (SELECT id FROM jobrunner_cron)');
            
            return new JobResponse(
                JobResponse::SUCCESS,
                print_r($msg, true),
                $msg
            );
            
        } else {
            
            return new JobResponse(
                JobResponse::SUCCESS,
                'Cron Logs not cleared, conn instance not compatible'
           );
            
        }
                
    }

    public function setOkDays($okDays)
    {
        $this->okDays = $okDays;
        return $this;
    }

    public function setKilledDays($killedDays)
    {
        $this->killedDays = $killedDays;
        return $this;
    }
}
