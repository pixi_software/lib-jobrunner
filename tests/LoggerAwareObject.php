<?php

namespace JobRunnerTest;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

class LoggerAwareObject implements LoggerAwareInterface
{
    
    public $logger;
    
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    public function runEcho($echo)
    {
        return $echo;
    }
    
    public function returnSome()
    {
        return 'some';
    }
    
    public function run() { return true; }
    
}
