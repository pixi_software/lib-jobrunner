<?php

use Pixi\Cronjob\Persistance\DoctrineDbalAdapter;
use Pixi\Cronjob\Job;
use Pixi\Cronjob\JobRunner;

class DoctrineDbalAdapterTest extends \PHPUnit_Framework_TestCase
{
    
    public $conn;
    
    public $adapter;
    
    public function setUp()
    {
        
        $config = new \Doctrine\DBAL\Configuration();
        
        $this->conn = \Doctrine\DBAL\DriverManager::getConnection(JobRunnerTestConfig::$config['db'], $config);
        
        $this->adapter = new DoctrineDbalAdapter($this->conn);
        
        $this->adapter->setupTables();
        
    }

    public function tearDown()
    {
    
        $this->conn->executeQuery('DROP table ' . $this->adapter->cronTable);
        $this->conn->executeQuery('DROP table ' . $this->adapter->jobTable);
    
    }
    
    public function populate()
    {
                
        $job1 = new Job(array('name' => 'date-job-1', 'context' => '\DateTime', 'function' => 'format', 'args' => array('d.m.Y H:i:s'), 'concurrency' => 1));
        
        $job2 = new Job(array('name' => 'date-job-2', 'context' => null, 'function' => function($some) { return number_format($some); }, 'args' => [500]));
        
        $job3 = new Job(array('name' => 'date-job-3', 'context' => false, 'function' => 'number_format', 'args' => [500]));
                
        $cron = new JobRunner();
        
        $cron->setPersistance($this->adapter);
        
        $cron->getPersistance()->setupTables();
        
        $cron->addJob($job1);
        $cron->addJob($job2);
        $cron->addJob($job3);
                
        $cron->execute();
                
    }
    
    public function testFakes()
    {
        
        $this->adapter->updateCron([]);
        $this->adapter->updateJob([]);
        
    }
    
    public function testTablesCreated()
    {
        
        $rs = $this->conn->fetchAll('DESCRIBE ' . $this->adapter->cronTable);
        
        $this->assertCount(9, $rs);
        
        $rs = $this->conn->fetchAll('DESCRIBE ' . $this->adapter->jobTable);
        
        $this->assertCount(9, $rs);
        
    }
    
    public function testQuery()
    {
        
        $rs = $this->adapter->query('select * from ' . $this->adapter->cronTable);
        
        $this->assertSame(0, $rs);
        
        $rs = $this->adapter->query('select * from ' . $this->adapter->jobTable);
        
        $this->assertSame(0, $rs);
        
        $rs = $this->adapter->query('select * from ' . $this->adapter->jobTable . ' where id = ?', [1]);
        
        $this->assertSame(0, $rs);
        
        $this->testFakes();
        
    }
    
    public function testPopulation()
    {
        
        $this->populate();
        
        $rs = $this->conn->fetchColumn("select count(*) from " . $this->adapter->cronTable);
        
        $this->assertSame('1', $rs, '1 Cron is created');
        
        $rs = $this->conn->fetchColumn("select count(*) from " . $this->adapter->jobTable);
        
        $this->assertSame('3', $rs, '3 Jobs are created');        
        
    }
    
    public function testKillCrons()
    {
        
        $this->populate();
        
        $rs = $this->conn->fetchAll('SELECT * FROM ' . $this->adapter->cronTable);
        
        $this->assertCount(1, $rs, '1 Cron is found');
        $this->assertSame('0', $rs[0]['killed'], 'Cron is not killed');
        
        $this->adapter->killCrons([$rs[0]['id']]);
        
        $rs = $this->conn->fetchAll('SELECT * FROM ' . $this->adapter->cronTable);

        $this->assertSame('1', $rs[0]['killed'], 'Cron is killed');
        
    }
    
    public function testKillJobs()
    {
    
        $this->populate();
    
        $jobs = $this->conn->fetchAll('SELECT * FROM ' . $this->adapter->jobTable);
    
        $this->assertCount(3, $jobs, '3 Jobs are found');
        
        $ids = [];
        
        foreach($jobs as $job) {
            
            $this->assertSame('0', $job['killed'], 'Job is not killed');
            $ids[] = $job['id'];
            
        }
    
        $this->adapter->killJobs($ids);
    
        $jobs = $this->conn->fetchAll('SELECT * FROM ' . $this->adapter->jobTable);
        
        foreach($jobs as $job) {
            
            $this->assertSame('1', $job['killed'], 'Job is killed');
            
        }
    
    }
    
}
