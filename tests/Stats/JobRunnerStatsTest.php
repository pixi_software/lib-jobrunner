<?php
namespace JobRunnerTest\Stats;

use Pixi\Cronjob\Stats\JobRunnerStats;

class JobRunnerStatsTest extends \PHPUnit_Framework_TestCase
{

    public function testCreation()
    {
        
        $stats = new JobRunnerStats();
        
        $stats->setCrons($this->getCronEntriesNoActive('test-job'));
        
        $this->assertSame(date('Y-m-d'), $stats->date('Y-m-d'));
        
        $this->assertSame(0, $stats->getActiveCount());
        
    }
    
    
    public function testStatsCount()
    {
        
        $stats = new JobRunnerStats();
        
        $stats->setCrons($this->getCronEntriesSomeActive('test-job'));
        
        $this->assertSame(2, $stats->getActiveCount());
                
        $this->assertArraySubset([33, 30], $stats->killableCrons());
        
        $stats->crons['0']['ttl'] = 315360000; // 10 years from 2015
        
        $this->assertArraySubset([30], $stats->killableCrons());
        
        $this->assertCount(1, $stats->killableCrons());
        
               
    }
    
    
    public function getCronEntriesSomeActive($jobName)
    {
        
        return array(
            0 => array(
                'id' => '33',
                'id_cron' => '13',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:50:00',
                'date_end' => ''
            ),
            1 => array(
                'id' => '30',
                'id_cron' => '12',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:58',
                'date_end' => ''
            ),
            2 => array(
                'id' => '26',
                'id_cron' => '11',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:55',
                'date_end' => '2015-03-17 14:49:55'
            ),
            3 => array(
                'id' => '23',
                'id_cron' => '10',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:53',
                'date_end' => '2015-03-17 14:49:53'
            ),
            4 => array(
                'id' => '20',
                'id_cron' => '9',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:51',
                'date_end' => '2015-03-17 14:49:51'
            ),
            5 => array(
                'id' => '17',
                'id_cron' => '8',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:50',
                'date_end' => '2015-03-17 14:49:50'
            ),
            6 => array(
                'id' => '13',
                'id_cron' => '7',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:47',
                'date_end' => '2015-03-17 14:49:47'
            ),
            7 => array(
                'id' => '10',
                'id_cron' => '6',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:45',
                'date_end' => '2015-03-17 14:49:45'
            ),
            8 => array(
                'id' => '6',
                'id_cron' => '5',
                'name' => $jobName,
                'error' => '0',
                'killed' => 0,
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:40',
                'date_end' => '2015-03-17 14:49:40'
            ),
            9 => array(
                'id' => '3',
                'id_cron' => '4',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:01',
                'date_end' => '2015-03-17 14:49:01'
            )
        );
        
    }
    
    public function getCronEntriesNoActive($jobName)
    {
        return array(
            0 => array(
                'id' => '33',
                'id_cron' => '13',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:50:00',
                'date_end' => '2015-03-17 14:50:00'
            ),
            1 => array(
                'id' => '30',
                'id_cron' => '12',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:58',
                'date_end' => '2015-03-17 14:49:58'
            ),
            2 => array(
                'id' => '26',
                'id_cron' => '11',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:55',
                'date_end' => '2015-03-17 14:49:55'
            ),
            3 => array(
                'id' => '23',
                'id_cron' => '10',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:53',
                'date_end' => '2015-03-17 14:49:53'
            ),
            4 => array(
                'id' => '20',
                'id_cron' => '9',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:51',
                'date_end' => '2015-03-17 14:49:51'
            ),
            5 => array(
                'id' => '17',
                'id_cron' => '8',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:50',
                'date_end' => '2015-03-17 14:49:50'
            ),
            6 => array(
                'id' => '13',
                'id_cron' => '7',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:47',
                'date_end' => '2015-03-17 14:49:47'
            ),
            7 => array(
                'id' => '10',
                'id_cron' => '6',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:45',
                'date_end' => '2015-03-17 14:49:45'
            ),
            8 => array(
                'id' => '6',
                'id_cron' => '5',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:40',
                'date_end' => '2015-03-17 14:49:40'
            ),
            9 => array(
                'id' => '3',
                'id_cron' => '4',
                'name' => $jobName,
                'killed' => 0,
                'error' => '0',
                'ttl' => '120',
                'date_start' => '2015-03-17 14:49:01',
                'date_end' => '2015-03-17 14:49:01'
            )
        );
    }
}

