<?php

namespace JobRunnerTest\Job;

use Pixi\Cronjob\Job;
use Pixi\Cronjob\JobRunner;
use Pixi\Cronjob\JobResponse;
use Pixi\Cronjob\Persistance\ArrayAdapter;
use Pixi\Cronjob\Exception\JobRunnerException;

class JobRunnerTest extends \PHPUnit_Framework_TestCase
{
    
    public function testOptionsSingle()
    {
        
        $options = [
            'name'          => 'cron-test-name',
            'ttl'           => 120,
            'log-data'      => false,
            'concurrency'   => 1,
            'persistance'   => 'fake persistance'
        ];
        
        $runner = new JobRunner($options);
        
        $this->assertSame(1, $runner->getConcurrency());
        
        $this->assertSame(120, $runner->getTTL());
        
        $this->assertSame('cron-test-name', $runner->getName());
        
        $this->assertSame('fake persistance', $runner->getPersistance());
        
        $runner->setId(1);
        
        $this->assertSame(1, $runner->getId());
        
        $this->assertArraySubset(['ttl' => 120, 'name' => 'cron-test-name', 'log-data' => false, 'concurrency' => 1], $runner->getOptions());
        
        $this->assertArrayNotHasKey('date_end', $runner->extract());
        $this->assertArrayNotHasKey('report', $runner->extract());
        $this->assertArrayNotHasKey('log-data', $runner->extract());
        
        $runner->dateEnd = date('Y-m-d H:i:s');
        
        $this->assertArrayHasKey('date_end', $runner->extract());
        
    }
    
    public function testAddJob()
    {
        
        $options = [
            'name'          => 'cron-test-name',
            'ttl'           => 120,
            'log-data'      => true,
            'concurrency'   => 1,
            'persistance'   => new ArrayAdapter()
        ];
        
        $runner = new JobRunner($options);
        
        $runner->addJob(new Job(['ttl' => 120, 'function' => 'date',  'name' => 'test-job1'])); // with 1 error
        $runner->addJob(new Job(['ttl' => 120, 'function' => 'microtime', 'name' => 'test-job2']));
        $runner->addJob(new Job(['ttl' => 120, 'function' => 'time', 'name' => 'test-job3']));
        
        try {
            $runner->addJob(new Job(['ttl' => 120, 'function' => 'time'])); // should not be executed
        } catch(JobRunnerException $e) {
            $this->assertSame('Job not valid', $e->getMessage());
        }
        
        $this->assertSame(480, $runner->getTTL());
        
        $runner->execute();
        
        $this->assertSame(JobResponse::EXCEPTION, $runner->jobReturnValues['test-job1']->getStatus());
        $this->assertSame(JobResponse::SUCCESS, $runner->jobReturnValues['test-job2']->getStatus());
        $this->assertSame(JobResponse::SUCCESS, $runner->jobReturnValues['test-job3']->getStatus());
        
    }
    
    public function testJobExpressionHandling()
    {
    
        $options = [
            'name'          => 'cron-test-name',
            'ttl'           => 120,
            'log-data'      => true,
            'concurrency'   => 1,
            'persistance'   => new ArrayAdapter()
        ];

        $runner = new JobRunner($options);

        $job = new Job(['ttl' => 120, 'function' => 'date',  'name' => 'test-job1']);
        $job->addExpression('not-working-expression', '1 == 2', true);
        
        $runner->addJob($job);

        $runner->execute();
        
        $this->assertSame(0, $runner->jobStatistics['test-job1']['executed']);
   
    }
    
    public function testJobBreakOnError()
    {
        
        $options = [
            'name'          => 'cron-test-name',
            'ttl'           => 120,
            'log-data'      => true,
            'concurrency'   => 1,
            'persistance'   => new ArrayAdapter()
        ];
        
        $runner = new JobRunner($options);
        
        $job1 = new Job(['ttl' => 120, 'function' => 'date', 'name' => 'test-job1', 'breakOnError' => true]); // will fail
        $job2 = new Job(['ttl' => 120, 'function' => 'time', 'name' => 'test-job2']); // should not be executed because of exit on error
        
        $runner->addJob($job1);
        $runner->addJob($job2);
        
        $runner->execute();
        
        $this->assertCount(1, $runner->getPersistance()->jobTable);
        
        $this->assertTrue($runner->isExecuted());
        
    }
    
    public function testRunnerExpressionFailure()
    {
    
        $options = [
            'name'          => 'cron-test-name',
            'ttl'           => 120,
            'log-data'      => true,
            'concurrency'   => 1,
            'persistance'   => new ArrayAdapter()
        ];
    
        $runner = new JobRunner($options);
    
        $runner->addExpression('failing-expression', '1 == 2', true);
        
        $job1 = new Job(['ttl' => 120, 'function' => 'date', 'name' => 'test-job1']); // will fail
        $job2 = new Job(['ttl' => 120, 'function' => 'time', 'name' => 'test-job2']); // should not be executed because of exit on error
    
        $runner->addJob($job1);
        $runner->addJob($job2);
    
        $this->assertFalse($runner->canExecuteRunner());
        
        $runner->execute();
        
        $this->assertFalse($runner->isExecuted());
        
    }
    
}
