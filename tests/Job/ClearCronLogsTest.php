<?php

use Pixi\Cronjob\Persistance\DoctrineDbalAdapter;
use Pixi\Cronjob\Job;
use Pixi\Cronjob\JobRunner;
use Pixi\Cronjob\Job\ClearCronLogs;
use Pixi\Cronjob\JobResponse;

class ClearCronLogsTest extends \PHPUnit_Framework_TestCase
{
    
    public $conn;
    
    public $adapter;
    
    public function setUp()
    {
        
        $config = new \Doctrine\DBAL\Configuration();
        
        $this->conn = \Doctrine\DBAL\DriverManager::getConnection(JobRunnerTestConfig::$config['db'], $config);
        
        $this->adapter = new DoctrineDbalAdapter($this->conn);
        
        $this->adapter->setupTables();
        
    }

    public function tearDown()
    {
    
        $this->conn->executeQuery('DROP table ' . $this->adapter->cronTable);
        $this->conn->executeQuery('DROP table ' . $this->adapter->jobTable);
    
    }
    
    public function populate()
    {
    
        $job1 = new Job(array('name' => 'date-job-1', 'context' => '\DateTime', 'function' => 'format', 'args' => array('d.m.Y H:i:s'), 'concurrency' => 1));
    
        $job2 = new Job(array('name' => 'date-job-2', 'context' => null, 'function' => function($some) { return number_format($some); }, 'args' => [500]));
    
        $job3 = new Job(array('name' => 'date-job-3', 'context' => false, 'function' => 'number_format', 'args' => [500]));
    
        $cron = new JobRunner();
    
        $cron->setPersistance($this->adapter);
    
        $cron->getPersistance()->setupTables();
    
        $cron->addJob($job1);
        $cron->addJob($job2);
        $cron->addJob($job3);
    
        $cron->execute();

    }
    
    public function testValidJobResponse()
    {
        
        $job = new ClearCronLogs($this->adapter);
        
        $job->setOkDays(10);
        $this->assertSame(10, $job->okDays, 'Ok days are set');
        
        $job->setKilledDays(10);
        $this->assertSame(10, $job->killedDays, 'Killed days are set');
        
        $job->run();

        $expected = [
            'okDays'        => 10,
            'killedDays'    => 10,
            'goodCrons'     => 0,
            'killedCrons'   => 0,
            'jobs'          => 0
        ];
        
        $this->assertSame(JobResponse::SUCCESS, $job->getReturnValue()->getStatus());
        
        $this->assertArraySubset($expected, $job->getReturnValue()->getReturnValue(), 'Job response is correct');
        
    }
    
    public function testInvalidJobResponse()
    {
    
        $job = new ClearCronLogs(new stdClass());
    
        $job->run();
        
        $this->assertSame(JobResponse::SUCCESS, $job->getReturnValue()->getStatus());
    
        $this->assertSame('Cron Logs not cleared, conn instance not compatible', $job->getReturnValue()->getMessage());
    
    }
    
    public function testClearingOkDays()
    {
        
        // 3 cron entries and 9 job entries are created
        $this->populate();
        $this->populate();
        $this->populate();
        
        // modify entries
        $dateEnd = date('Y-m-d', strtotime('-4 days'));
        $this->conn->update($this->adapter->cronTable, ['date_end' => $dateEnd], ['id' => 1]);
        
        
        $job = new ClearCronLogs($this->adapter);
        
        $job->setOkDays(3)->setKilledDays(10);
        
        $job->run();
        
        $this->assertSame(JobResponse::SUCCESS, $job->getReturnValue()->getStatus());
        
        $expected = [
            'okDays'        => 3,
            'killedDays'    => 10,
            'goodCrons'     => 1,
            'killedCrons'   => 0,
            'jobs'          => 3
        ];
        
        $this->assertArraySubset($expected, $job->getReturnValue()->getReturnValue(), 'Job cleared 1 cron and 3 jobs');
        
    }
    
    public function testClearingKilledDays()
    {
    
        // 3 cron entries and 9 job entries are created
        $this->populate();
        $this->populate();
        $this->populate();
    
        // modify entries
        $dateEnd = date('Y-m-d', strtotime('-11 days'));
        $this->conn->update($this->adapter->cronTable, ['date_end' => $dateEnd, 'killed' => 1], ['id' => 1]);
    
    
        $job = new ClearCronLogs($this->adapter);
    
        $job->setOkDays(3)->setKilledDays(10);
    
        $job->run();
    
        $this->assertSame(JobResponse::SUCCESS, $job->getReturnValue()->getStatus());
    
        $expected = [
            'okDays'        => 3,
            'killedDays'    => 10,
            'goodCrons'     => 0,
            'killedCrons'   => 1,
            'jobs'          => 3
        ];
    
        $this->assertArraySubset($expected, $job->getReturnValue()->getReturnValue(), 'Job cleared 1 cron and 3 jobs');
    
    }
    
    public function testClearingKilledOkDays()
    {
    
        // 3 cron entries and 9 job entries are created
        $this->populate();
        $this->populate();
        $this->populate();
    
        // modify entries
        $dateEnd = date('Y-m-d', strtotime('-4 days'));
        $this->conn->update($this->adapter->cronTable, ['date_end' => $dateEnd, 'killed' => 1], ['id' => 1]);
        $this->conn->update($this->adapter->cronTable, ['date_end' => $dateEnd], ['id' => 2]);
    
    
        $job = new ClearCronLogs($this->adapter);
    
        $job->setOkDays(3)->setKilledDays(3);
    
        $job->run();
    
        $this->assertSame(JobResponse::SUCCESS, $job->getReturnValue()->getStatus());
    
        $expected = [
            'okDays'        => 3,
            'killedDays'    => 3,
            'goodCrons'     => 1,
            'killedCrons'   => 1,
            'jobs'          => 6
        ];
    
        $this->assertArraySubset($expected, $job->getReturnValue()->getReturnValue(), 'Job cleared 2 cron and 6 jobs');
    
    }
   
}
