<?php

namespace JobRunnerTest\Job;

use Pixi\Cronjob\Job;
use Pixi\Cronjob\JobResponse;

class JobTest extends \PHPUnit_Framework_TestCase
{
    
    public function testOptionsSingle()
    {
        
        $job = new Job(['name' => 'test-job', 'ttl' => 520]);
        
        // name
        $this->assertSame('test-job', $job->getName());
        
        // TTL
        $this->assertSame(520, $job->getTTL());
        $job->setTTL(521);
        $this->assertSame(521, $job->getTTL());
        $job->setTTL('some');
        $this->assertSame(521, $job->getTTL());
        
        
        // concurrency
        $this->assertSame(1, $job->getConcurrency());
        $job->setConcurrency(2);
        $this->assertSame(2, $job->getConcurrency());
        
        // id_cron
        $job->setIdCron(1);
        $this->assertSame(1, $job->getIdCron());
        
        // id
        $job->setId(1);
        $this->assertSame(1, $job->getId());
        
        // error
        $this->assertFalse($job->hasError());
        $job->statistics['has.error'] = 1;
        $this->assertTrue($job->hasError());
        
        // break on error
        $this->assertFalse($job->breakOnError());
        
        $this->assertFalse($job->isValid());
        
    }
    
    public function testOptionsSetter()
    {
        
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true
        );
        
        $job = new Job($settings);
        
        $this->assertTrue($job->breakOnError());
        
        $this->assertSame(2, $job->getConcurrency());
        
        $this->assertSame('test-job', $job->getName());
        
        $this->assertSame(520, $job->getTTL());
        
        $this->assertArraySubset($settings, $job->getOptions());
        
        $this->assertEmpty($job->getReturnValue());
        
    }
    
    public function testLogger()
    {
        
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true
        );
        
        $job = new Job($settings);
        
        $logger = $job->getLogger();
        
        $this->assertInstanceOf('\Pixi\Cronjob\ArrayLogger', $logger);
        
        $this->assertSame(0, count($logger->getFormattedLogs()));
        
        $logger->info('unit-testing', ['test-context']);
        
        $this->assertSame(1, count($logger->getFormattedLogs()));
        
        $testLog = array(
            'level'     => 'info',
            'message'   => 'unit-testing',
            'context'   => '["test-context"]'
        );
        
        $this->assertArraySubset($testLog, $job->getLogEntries()[0]);
        
    }
    
    public function testStatistics()
    {
        
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true
        );
        
        $job = new Job($settings);
        
        $statistics = array(
            'execution.name'      => '',
            'date.start'    => 0,
            'date.end'      => 0,
            'time.total'    => '0.00 sec',
            'memory.start'  => 0,
            'memory.end'    => 0,
            'memory.usage'  => '0 kb',
            'executed'      => 0,
            'has.error'     => 0,
            'error.message' => ''
        );
        
        $this->assertArraySubset($statistics, $job->getStatistics());
        
    }
    
    
    /**
     * Test Object Creation from text
     */
    public function testInitObject()
    {
        
        // ok class
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'context'       => '\DateTime', 
            'function'      => 'format', 
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertSame('Object', $job->options['contextType']);
        $this->assertSame('DateTime::format', $job->statistics['execution.name']);
        
        $this->assertTrue($job->isValid());
        
        // fake class
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'context'       => '\DateTimeNotExisting',
            'function'      => 'format',
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertFalse($job->options['contextType']);
        
        $this->assertFalse($job->isValid());
        
        // from object
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'context'       => new \DateTime(),
            'function'      => 'format',
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertSame('Object', $job->options['contextType']);
        $this->assertSame('DateTime::format', $job->statistics['execution.name']);
        
        $this->assertTrue($job->isValid());
        
        // from logger aware object
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'context'       => '\JobRunnerTest\LoggerAwareObject',
            'function'      => 'run'
        );
        
        $job = new Job($settings);
        
        $this->assertSame('Object', $job->options['contextType']);
        $this->assertSame('JobRunnerTest\LoggerAwareObject::run', $job->statistics['execution.name']);
        
        $this->assertInstanceOf('Psr\Log\LoggerInterface', $job->options['context']->logger);
        
        $this->assertTrue(is_array($job->options['context']->logger->getLogs()));
        
        $this->assertTrue($job->isValid());
        
    }
    
    public function testInitFunction()
    {
        
        // ok function
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'function'      => 'date',
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertSame('Function', $job->options['contextType']);
        $this->assertSame('Function::date', $job->statistics['execution.name']);
        
        $this->assertTrue($job->isValid());
        
        // fake function
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'function'      => 'nonExistingFunction',
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertFalse($job->options['contextType']);
        
        $this->assertFalse($job->isValid());
        
    }
    
    public function testInitCallable()
    {
    
        // ok function
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'function'      => function() { return true; },
            'args'          => array('d.m.Y H:i:s')
        );
    
        $job = new Job($settings);
    
        $this->assertSame('Closure', $job->options['contextType']);
        $this->assertSame('Closure::anonymous', $job->statistics['execution.name']);
    
        $this->assertTrue($job->isValid());
    
        // fake function
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'args'          => array('d.m.Y H:i:s')
        );
    
        $job = new Job($settings);
    
        $this->assertFalse($job->options['contextType']);
    
        $this->assertFalse($job->isValid());
    
    }
    
    public function testExtract()
    {
        
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'function'      => function() { return true; },
            'args'          => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $this->assertArraySubset(
            ['name' => 'test-job', 'id_cron' => false, 'ttl' => 520, 'error' => false], 
            $job->extract()
        );
        
        $this->assertArraySubset(
            ['name' => 'test-job', 'ttl' => 520, 'error' => false], 
            $job->extract(['id_cron', 'data'])
        );
        
        $job->setId(5);
        $job->dateEnd = '2015-15-03 00:00:00';
        
        $this->assertArraySubset(
            ['name' => 'test-job', 'ttl' => 520, 'error' => false, 'id' => 5, 'date_end' => '2015-15-03 00:00:00'], 
            $job->extract(['id_cron', 'data'])
        );
        
        $job->setOptions(['add_context' => true]);
        
        $data = json_decode($job->extract(false)['data'], true);
        
        $this->assertArrayHasKey('context', $data['options']);
        
        $job->setOptions(['add_context' => false]);
        
        $data = json_decode($job->extract(false)['data'], true);
        
        $this->assertFalse(isset($data['options']['context']));
                
    }
    
    public function testRunException()
    {
        
        $settings = array(
            'ttl'           => 520,
            'name'          => 'test-job',
            'concurrency'   => 2,
            'breakOnError'  => true,
            'function'      => function() { return true; },
            'args'          => array('d.m.Y H:i:s', 'throwException' => 'Forced Throw Test')
        );
        
        $job = new Job($settings);
        
        $job->run();
        
        $this->assertTrue($job->hasError());
        
        $returnValue = $job->getReturnValue();
        
        $this->assertInstanceOf('Pixi\Cronjob\JobResponse', $returnValue);
        
        $this->assertSame('Forced Throw Test', $returnValue->getMessage());
        
        $this->assertSame(\Pixi\Cronjob\JobResponse::EXCEPTION, $returnValue->getStatus());
        
        $this->assertInstanceOf('Pixi\Cronjob\Exception\JobException', $returnValue->getReturnValue()['exception']);
        
        $this->assertArrayHasKey('status', $returnValue->jsonSerialize());
        $this->assertArrayHasKey('message', $returnValue->jsonSerialize());
        $this->assertArrayHasKey('returnValue', $returnValue->jsonSerialize());

    }
    
    public function testRunClosure()
    {
        
        $settings = array(
            'name'      => 'test-job',
            'function'  => function() { return 'test return value'; },
            'args'      => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $job->run();
        
        $this->assertFalse($job->hasError());
        
        $returnValue = $job->getReturnValue();
        
        $this->assertSame('test return value', $returnValue->getReturnValue());
        
        $this->assertSame(\Pixi\Cronjob\JobResponse::SUCCESS, $returnValue->getStatus());
        
        
        // Response object as return value with error
        $settings = array(
            'name'      => 'test-job',
            'function'  => function() { return new JobResponse(2, 'Job Executed ERROR', true); },
            'args'      => array('d.m.Y H:i:s')
        );
        
        $job = new Job($settings);
        
        $job->run();
        
        $this->assertTrue($job->hasError());
        
        $returnValue = $job->getReturnValue();
        
        $this->assertTrue($returnValue->getReturnValue());
        
        $this->assertSame(\Pixi\Cronjob\JobResponse::ERROR, $returnValue->getStatus());
        
        $this->assertSame('Job Executed ERROR', $returnValue->getMessage());
        
    }
    
    public function testRunObject()
    {
    
        // with args
        $settings = array(
            'name'      => 'test-job',
            'context'   => '\JobRunnerTest\LoggerAwareObject',
            'function'  => 'runEcho',
            'args'      => ['ECHO test']
        );
    
        $job = new Job($settings);
    
        $job->run();
        
        $this->assertFalse($job->hasError());
    
        $returnValue = $job->getReturnValue();
    
        $this->assertSame('ECHO test', $returnValue->getReturnValue());
    
        $this->assertSame(\Pixi\Cronjob\JobResponse::SUCCESS, $returnValue->getStatus());
        
        
        // with no args
        $settings = array(
            'name'      => 'test-job',
            'context'   => '\JobRunnerTest\LoggerAwareObject',
            'function'  => 'returnSome'
        );
        
        $job = new Job($settings);
        
        $job->run();
        
        $this->assertFalse($job->hasError());
        
        $returnValue = $job->getReturnValue();
        
        $this->assertSame('some', $returnValue->getReturnValue());
        
        $this->assertSame(\Pixi\Cronjob\JobResponse::SUCCESS, $returnValue->getStatus());
    
    }
    
    public function testRunNoContext()
    {
    
        // with args
        $settings = array(
            'name'      => 'test-job',
            'context'   => '\JobRunnerTest\LoggerAwareObject\Fake',
            'function'  => 'runEcho',
            'args'      => ['ECHO test']
        );
    
        $job = new Job($settings);
    
        $job->run();
    
        $this->assertTrue($job->hasError());
    
        $returnValue = $job->getReturnValue();
    
        $this->assertFalse($returnValue->getReturnValue());
    
        $this->assertSame(\Pixi\Cronjob\JobResponse::NOT_EXECUTED, $returnValue->getStatus());
    
    }
    
    public function testJobTraits()
    {
        
        $settings = array(
            'name'      => 'test-job',
            'context'   => '\JobRunnerTest\LoggerAwareObject',
            'function'  => 'returnSome'
        );
        
        $job = new Job($settings);
        
        $this->assertArrayHasKey('concurrency', $job->getExpressions());
        
        $job->addExpression('test-expression', 'Expression', 'Value');
        
        $this->assertArraySubset(['test-expression' => ['expression' => 'Expression', 'value' => 'Value']], $job->getExpressions());
        
        $job->setExpressions(['test-expression' => ['expression' => 'Expression', 'value' => 'Value']]);
        $this->assertArraySubset(['test-expression' => ['expression' => 'Expression', 'value' => 'Value']], $job->getExpressions());
        
        $this->assertArrayNotHasKey('concurrency', $job->getExpressions());
        
    }
    
    public function testJobIsValid()
    {
        
        $job = new Job(['ttl' => 120, 'function' => 'date']);
        
        $this->assertFalse($job->isValid());
        
    }
    
}
