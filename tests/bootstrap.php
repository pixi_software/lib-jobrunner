<?php

require_once 'vendor/autoload.php';

class JobRunnerTestConfig
{
    
    public static $config = [
        'db' => [
            'dbname'    => 'cron_test',
            'user'      => 'root',
            'host'      => 'localhost',
            'driver'    => 'pdo_mysql'
        ]
    ];
    
}